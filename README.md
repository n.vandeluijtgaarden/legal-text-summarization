# Automatic Summarization of Legal Text

This repository contains all code and data belonging to the paper _Abstractive Summarization of Court Verdicts Using Sequence-to-sequence Models_ by Nick van de Luijtgaarden, Marijn Schraagen and Floris Bex, and the [MSc Thesis](https://dspace.library.uu.nl/handle/1874/384802) by Nick van de Luijtgaarden on which the paper was based.

The `data` folder contains a sample of the [Rechtspraak](https://www.rechtspraak.nl/Uitspraken/Paginas/Open-Data.aspx) dataset as well as generated summaries. The `log` folder is where all loggings of the `pipeline.py` file are saved. The `model` folder contains the source code of the implementation of the model of [Chen & Bansal, 2018](https://arxiv.org/abs/1805.11080). However, we have modified their model to work with the newer version of PyTorch, as well as to better align with the pipeline of this research. Finally, the `packages` folder contains the files needed for using Stanford `CoreNLP`, `ROUGE` and `Ucto`.

## Testing environment

Experiments are conducted on a **p3.2x large** AWS instance: a machine running **Ubuntu 18.04.2 LTS** 64-bit with 61GB RAM, **Intel Xeon E5-2686 v4 @ 2.30GHz CPU** and an **Nvidia Tesla V100 SXM2 16GB**. **We do not recommend using a virtual machine for running Ubuntu**, as it makes it extremely difficult to access your GPU, which is needed to make the training times acceptable. **For decoding, a GPU is neccessary too, as it will take a very long time to generate summaries**.

## Dependencies

* [Python 3.7.3](https://www.python.org/downloads/release/python-373/)
* [PyTorch 1.1.0](https://github.com/pytorch/pytorch) (GPU and CUDA 10 enabled installation)
* [virtualenv](https://github.com/pypa/virtualenv)
* [gensim](https://github.com/RaRe-Technologies/gensim)
* [cytoolz](https://github.com/pytoolz/cytoolz)
* [tensorboardX](https://github.com/lanpa/tensorboard-pytorch)
* [pyrouge](https://github.com/bheinzerling/pyrouge)

For a full overview of all dependencies, please refer to the [requirements file](https://git.science.uu.nl/n.vandeluijtgaarden/legal-text-summarization/blob/develop/requirements.txt).

### Set-up

1. Install `Python 3.7.3`

```bash 
sudo apt-get install build-essential checkinstall libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev 

cd /usr/src 
sudo wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz 
sudo tar xzf Python-3.7.3.tgz 

cd Python-3.7.3 
sudo ./configure --enable-optimizations 
sudo make altinstall 
``` 

2.  Install `virtualenv`, `python3-pip`, `default-jre` and `libxml-parser-perl`

```bash
sudo apt install virtualenv python3-pip default-jre libxml-parser-perl
```

3. Set-up a virtual environment

```bash
 virtualenv --python=python3.7 env
```

4. Activate environment variables by going to the `env/bin` directory and editing your `activate` file. At the end of the file, point the `CLASSPATH`, `DATA` and `ROUGE` variables to the location of the respective files

```bash
export CLASSPATH=~/legal-text-summarization/packages/stanford_corenlp/stanford-corenlp-3.9.2.jar
export DATA=~/legal-text-summarization/data/rechtspraak/finished
export ROUGE=~/legal-text-summarization/packages/rouge/
```

5.  Add the following line of code to the `deactivate()` function in the `activate` file

```bash
unset CLASSPATH
unset DATA
unset ROUGE
```

6. Activate your environment and install requirements. Also, download the `punkt` package from `NLTK` in your environment

```bash
source env/bin/activate
pip install -r requirements.txt

python
import nltk
nltk.download('punkt')
exit()
```

7. Check if the tokenizer is working and compare the result with the outcome below.

```bash
echo "Hi Hello." | java edu.stanford.nlp.process.PTBTokenizer
```
  
```bash
Hi
Hello
.
PTBTokenizer tokenized 3 tokens at 41.85 tokens per second.
```  

8. The newest model makes use of the `ucto` tokenizer. In the `packages` directory of this repository, four packages can be found that need to be installed (`libfolia-2.0`, `ticcutils-0.21`, `ucto-0.16` and `uctodata-0.8`). This can take some time to install. First, install necessary packages:

```bash
sudo apt-get install libicu-dev libxml2-dev libexttextcat-dev autotools-dev libtool pkg-config autoconf-archive libboost-dev libboost-regex-dev libtar-dev libbz2-dev zlib1g-dev 
```

10. Now, all four of the packages need to be installed in the following order: `ticcutils-0.21`, `libfolia-2.0`, `uctodata-0.8` and `ucto-0.16`. A package needs to be installed in the way that is done below. **Note that this needs to be done for all four packages.**

```bash
cd packages/ticcutils-0.21
Sudo bash bootstrap.sh 
Sudo chmod +x configure 
Sudo ./configure 
Sudo make 
Sudo make install 
sudo make check 
```

11. Check if ucto is running by running the command below:

```bash
ucto
```

```bash
ucto: missing a language specification (-L or --detectlanguages or --uselanguages option)
ucto: Available Languages: deu,eng,fra,fry,generic,ita,nld,nld-historical,nld-sonarchat,nld-twitter,nld-withplaceholder,por,rus,spa,swe,tur,
```

### Running the model

12. Running the file always starts with `python pipeline.py`. Options are either all/pre/learn/decode/eval. Examples:

```bash
python pipeline.py all --output_cases=data/rechtspraak/cases --minWords=40 --maxWords=150 --minSentences=3 --maxSentences=6 --minWordsInSentence=7 --multiprocess --dataExploration --batch_size=32 --ckpt_freq=300 --beam=5 --data_split=test --decode_dir=data/rechtspraak/output --decode_batch_size=32
python pipeline.py learn  --batch_size=32 --ckpt_freq=300
python pipeline.py pre  --output_cases=data/rechtspraak/cases --minWords=40 --maxWords=150 --minSentences=3 --maxSentences=6 --minWordsInSentence=7 --multiprocess --dataExploration
python pipeline.py decode --beam=5 --data_split=test --decode_dir=data/rechtspraak/output --decode_batch_size=32
python pipeline.py eval 
```

**Data loading and filtering (pre)**
* output_cases: Output directory for verdicts and summaries
* minWords: Minimal number of words needed in a summary for it to be added to the training set
* maxWords: Maximum number of words needed in a summary for it to be added to the training set
* minSentences: Minimal number of sentence needed in a summary for it to be added to the training set
* maxSentences: Maximum number of sentence needed in a summary for it to be added to the training set
* minWordsInSentence: Minimal number of words needed in each summary sentence for it to be added to the training set (> 6)
* min
* (not required, default= all cases) minYear: Minimal year of cases
* (not required, default= all cases) maxYear: Maximum year of cases
* (not required, default= all cases) caseType: Filter for case category
* (not required, default=False) multiprocess: If set, multiprocessing will be used to make extraction labels. Can give memory problems on low-end PCs
* (not required, default=False) dataExploration: If set, application will log information about the dataset
* (not required, default=False) nltk: If parameter is added, the application will use NLTK for tokenization in pre-processing

**Training options (learn)**
* batch_size: Number of training examples utilized in one iteration for RL
* ckpt_freq: Number of steps needed to save a checkpoint in RL model 

**Decoding options (decode)**
* beam: Number of hypothesis for beam search (1-5). A beam size of > 1 can lead to memory problems on low-end PCs
* data_split: Run decoding on either test or validation set (test/val)
* decode_dir: Output directory for generated summaries
* decode_batch_size: Batch size for decoding

### Additional notes

* CUDA out of memory problems? Try to lower batch size, reboot the computer or just run the application again. This is a problem related to the model of Chen and can happen quite randomly.
* You will get errors like: Untokenizable:  (U+F02D, decimal: 61485). This is fine.

* One-sentence summaries are currently [not supported](https://github.com/ChenRocks/fast_abs_rl/issues/37).

* Getting an error related to the kernel and input size, like [this one](https://discuss.pytorch.org/t/kernel-size-cant-greater-than-actual-input-size/21579)? Try to increase the number of words needed in a sentence (minWordsInSentence to e.g. 7), as the convolutional network looks at context of 3-5 words. If this does not work, try increasing the batch size (batch_size to e.g. 32).

* Getting the following error? OSError: [Errno 12] Cannot allocate memory. Clean up the memory:

```bash
ps -ef | grep -i -e pytho && pkill -f legal-text-summarization
```
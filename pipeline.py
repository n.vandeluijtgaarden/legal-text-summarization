import argparse
import os
import shutil
import json
import pandas as pd

from time import time
from datetime import timedelta, datetime
from os.path import exists

from models.chen_bansal_2018.train_word2vec import trainWord2Vec
from models.chen_bansal_2018.make_extraction_labels import makeExtractionLabels, makeExtractionLabelsMP
from models.chen_bansal_2018.train_abstractor import trainAbstractor
from models.chen_bansal_2018.train_extractor_ml import trainExtractor
from models.chen_bansal_2018.train_full_rl import trainFullRL
from models.chen_bansal_2018.decode_full_model import decodeModel
from models.chen_bansal_2018.make_eval_references import makeEvaluationReferences
from models.chen_bansal_2018.eval_full_model import evaluateFullModel
from nltk.util import ngrams
from collections import Counter

from dataloader import loadAndFilterData
from preprocessing import tokenize
from exploration import exploreCsv, exploreCases, exploreGeneratedSummaries

currentTime = datetime.fromtimestamp(time()).strftime('%Y-%m-%d-%H:%M:%S')
log_dir = f'log/{currentTime}'
csv_file = 'data/rechtspraak/final.csv'

class Experiment:
    def __init__(self, name, minYear, maxYear, caseType, decode_dir):
        self.name = name
        self.minYear = minYear
        self.maxYear = maxYear
        self.caseType = caseType
        self.decode_dir = decode_dir

def _preprocess(args):
   with open(f'{log_dir}/preprocess', 'w') as f:                           
        partial_time = time()
        print('Starting loadAndFilterData...')
        loadAndFilterData(args.output_cases, args.minWords, args.maxWords, args.minSentences, args.maxSentences, args.minWordsInSentence, args.minYear, args.maxYear, args.caseType, args.nltk)
        f.write(f'loadAndFilterData: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Parameters: minWords {args.minWords} - maxWords {args.maxWords} - minSentences {args.minSentences} - maxSentences {args.maxSentences} - minWordsinSentence {args.minWordsInSentence} - minYear {args.minYear} - maxYear {args.maxYear} - type {args.caseType} - nltk {args.nltk} \n\n')
        
        if args.dataExploration:
                partial_time = time()
                print('Starting exploration...\n')
                #exploreCsv(f) # before pre-processing
                exploreCases(f, args.output_cases) # after pre-processing
                f.write(f'exploreCsv & exploreCases: {timedelta(seconds=time()-partial_time)}\n\n')   
        
        print('Starting tokenization...\n')
        partial_time = time()
        tokenize(args.output_cases)
        f.write(f'tokenize: {timedelta(seconds=time()-partial_time)}\n\n')

        print('Starting Word2Vec...\n')        
        partial_time = time()       
        vocab_length = trainWord2Vec('data/rechtspraak/word2vec')
        f.write(f'trainWord2Vec: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Vocabulary length: {vocab_length}\n\n')
  
        if args.multiprocess:
                print('Starting makeExtractionLabelsMP...\n')
                partial_time = time()
                makeExtractionLabelsMP()
                f.write(f'makeExtractionLabelsMP: {timedelta(seconds=time()-partial_time)}\n\n')   
        else:
                print('Starting makeExtractionLabels...\n')
                partial_time = time()
                makeExtractionLabels()
                f.write(f'makeExtractionLabels: {timedelta(seconds=time()-partial_time)}\n\n')     

def _learn(args):
   with open(f'{log_dir}/learn', 'w') as f: 
        word2vec = ''  
        with os.scandir('data/rechtspraak/word2vec/') as entries:
                for entry in entries:
                        if entry.name.endswith(".bin"):
                                word2vec = entry.name
                                break      
        '''if word2vec == '':
                raise ValueError('No word2Vec file found in directory data/rechtspraak/word2vec/')

        if os.path.exists('data/rechtspraak/extractor'):
                raise ValueError('Extractor folder already exists!')
        if os.path.exists('data/rechtspraak/abstractor'):
                raise ValueError('Abstractor folder already exists!')
        if os.path.exists('data/rechtspraak/rl/'):
                raise ValueError('RL folder already exists!')
        
        print('Starting trainExtractor...\n')        
        partial_time = time()
        trainExtractor('data/rechtspraak/extractor', f'data/rechtspraak/word2vec/{word2vec}', batch=32)
        f.write(f'trainExtractor: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Parameters: batch_size 32\n\n')
        
        print('Starting trainAbstractor...\n')        
        partial_time = time()
        trainAbstractor('data/rechtspraak/abstractor', f'data/rechtspraak/word2vec/{word2vec}', batch=32)
        f.write(f'trainAbstractor: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Parameters: batch_size 32\n\n')   '''
        
        print('Starting trainFullRL...\n')        
        partial_time = time()     
        trainFullRL('data/rechtspraak/abstractor', 'data/rechtspraak/extractor', 'data/rechtspraak/rl/', batch=args.batch_size, ckpt_freq=args.ckpt_freq)
        f.write(f'trainFullRL: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Parameters: ckpt_freq {args.ckpt_freq} -  batch_size {args.batch_size}\n\n')

def _decode(args):
   if os.path.exists(args.decode_dir):
       raise ValueError(f'{args.decode_dir} decode output folder already exists!')

   with open(f'{log_dir}/decode', 'w') as f:        
        print('Starting decodeModel...\n')        
        partial_time = time()
        decodeModel(args.decode_dir, 'data/rechtspraak/rl/', args.data_split, args.beam, batch=args.decode_batch_size)
        f.write(f'decodeModel: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Parameters: decode_dir {args.decode_dir} - data_split {args.data_split} - beam {args.beam} - batch {args.decode_batch_size}\n')

def _evaluate(args):
   with open(f'{log_dir}/evaluate', 'w') as f: 
        print('Starting makeEvaluationReferences...\n')        
        partial_time = time()
        makeEvaluationReferences()
        f.write(f'makeEvaluationReferences: {timedelta(seconds=time()-partial_time)}\n')

        print('Starting evaluateFullModel...\n')        
        partial_time = time()
        rouge = evaluateFullModel(args.decode_dir)
        f.write(f'evaluateFullModel: {timedelta(seconds=time()-partial_time)}\n')
        f.write(f'Rouge: {rouge}')

        print('Starting exploreGeneratedSummaries...\n')       
        partial_time = time()   
        exploreGeneratedSummaries(f, args.decode_dir)
        f.write(f'exploreGeneratedSummaries: {timedelta(seconds=time()-partial_time)}\n')
   
def _intersect(a, b):
      ''' calculates a - b '''
      intersection = Counter(a) & Counter(b)
      multiset_sum_without_common = Counter(a) - intersection
      elements = list(multiset_sum_without_common.elements())
      return elements

def _ngrams():
   one_grams_unique, two_grams_unique, three_grams_unique, four_grams_unique = 0,0,0,0
   one_grams_unique_ref, two_grams_unique_ref, three_grams_unique_ref, four_grams_unique_ref = 0,0,0,0
   one_grams_unique_ref_sum, two_grams_unique_ref_sum, three_grams_unique_ref_sum, four_grams_unique_ref_sum = 0,0,0,0
   
   one_grams_total, two_grams_total, three_grams_total, four_grams_total = 0,0,0,0
   one_grams_total_ref, two_grams_total_ref, three_grams_total_ref, four_grams_total_ref = 0,0,0,0
   one_grams_total_ref_sum, two_grams_total_ref_sum, three_grams_total_ref_sum, four_grams_total_ref_sum = 0,0,0,0
   for i in range(7152):
       
      # Get generated summary n-grams
      with open(f'data/rechtspraak/output_phase2_test/output/{i}.dec') as sum:   
         one_gram_sum, two_gram_sum, three_gram_sum, four_gram_sum = [],[],[],[]
         for index, line in enumerate(sum):      
            if (line != '\n'):
               one_gram_sum.extend(line.split())
               two_gram_sum.extend(list(ngrams(line.split(), 2)))
               three_gram_sum.extend(list(ngrams(line.split(), 3)))
               four_gram_sum.extend(list(ngrams(line.split(), 4)))

      # Get verdict n-grams & reference summary n-grams
      with open(f'data/rechtspraak/finished/test_all/{i}.json', 'r') as ver:
         one_gram_ver, two_gram_ver, three_gram_ver, four_gram_ver = [],[],[],[]
         one_gram_sum_ref, two_gram_sum_ref, three_gram_sum_ref, four_gram_sum_ref = [],[],[],[]
         data = json.loads(ver.read())
         verdict = data['verdict']
         summary = data['summary']  

         for index, line in enumerate(verdict):
            if (line != '\n'):
               one_gram_ver.extend(line.split())
               two_gram_ver.extend(list(ngrams(line.split(), 2)))
               three_gram_ver.extend(list(ngrams(line.split(), 3)))
               four_gram_ver.extend(list(ngrams(line.split(), 4)))

         for index, line in enumerate(summary):
            if (line != '\n'):
               one_gram_sum_ref.extend(line.split())
               two_gram_sum_ref.extend(list(ngrams(line.split(), 2)))
               three_gram_sum_ref.extend(list(ngrams(line.split(), 3)))
               four_gram_sum_ref.extend(list(ngrams(line.split(), 4)))

      # Calculate intersections with generated summary vs verdict
      one_grams_unique += len(_intersect(one_gram_sum, one_gram_ver))
      two_grams_unique += len(_intersect(two_gram_sum, two_gram_ver))
      three_grams_unique += len(_intersect(three_gram_sum, three_gram_ver))
      four_grams_unique += len(_intersect(four_gram_sum, four_gram_ver))

      # Calculate intersections with generated summary vs verdict
      one_grams_unique_ref += len(_intersect(one_gram_sum_ref, one_gram_ver))
      two_grams_unique_ref += len(_intersect(two_gram_sum_ref, two_gram_ver))
      three_grams_unique_ref += len(_intersect(three_gram_sum_ref, three_gram_ver))
      four_grams_unique_ref += len(_intersect(four_gram_sum_ref, four_gram_ver))

       # Calculate intersections with generated summary vs reference summary
      one_grams_unique_ref_sum += len(_intersect(one_gram_sum, one_gram_sum_ref))
      two_grams_unique_ref_sum += len(_intersect(two_gram_sum, two_gram_sum_ref))
      three_grams_unique_ref_sum += len(_intersect(three_gram_sum, three_gram_sum_ref))
      four_grams_unique_ref_sum += len(_intersect(four_gram_sum, four_gram_sum_ref))

      # Add up summary n-grams
      one_grams_total += len(one_gram_sum)
      two_grams_total += len(two_gram_sum)
      three_grams_total += len(three_gram_sum)
      four_grams_total += len(four_gram_sum)

      # Add up reference n-grams
      one_grams_total_ref += len(one_gram_sum_ref)
      two_grams_total_ref += len(two_gram_sum_ref)
      three_grams_total_ref += len(three_gram_sum_ref)
      four_grams_total_ref += len(four_gram_sum_ref)


   
   print(f'{round((one_grams_unique / one_grams_total)*100, 2)} unique 1-grams in all generated summaries.')
   print(f'{round((two_grams_unique / two_grams_total)*100, 2)} unique 2-grams in all generated summaries.')
   print(f'{round((three_grams_unique / three_grams_total)*100, 2)} unique 3-grams in all generated summaries.')
   print(f'{round((four_grams_unique / four_grams_total)*100, 2)} unique 4-grams in all generated summaries.\n')
      
   print(f'{round((one_grams_unique_ref / one_grams_total_ref)*100, 2)} unique 1-grams in all reference summaries.')
   print(f'{round((two_grams_unique_ref / two_grams_total_ref)*100, 2)} unique 2-grams in all reference summaries.')
   print(f'{round((three_grams_unique_ref / three_grams_total_ref)*100, 2)} unique 3-grams in all reference summaries.')
   print(f'{round((four_grams_unique_ref / four_grams_total_ref)*100, 2)} unique 4-grams in all reference summaries.')   

   print(f'{round((one_grams_unique_ref_sum / one_grams_total)*100, 2)} unique 1-grams in all generated summaries compared to reference summaries.')
   print(f'{round((two_grams_unique_ref_sum / two_grams_total)*100, 2)} unique 2-grams in all reference summaries compared to reference summaries.')
   print(f'{round((three_grams_unique_ref_sum / three_grams_total)*100, 2)} unique 3-grams in all reference summaries compared to reference summaries.')
   print(f'{round((four_grams_unique_ref_sum / four_grams_total)*100, 2)} unique 4-grams in all reference summaries compared to reference summaries.')   


         

         



   ''' 

   for summary in test_summaries


      find matching verdict
         compute n_grams summary (1,2,3,4)
         compute n_grams verdict (1,2,3,4)

         compute overlap
         n-grams summary - overlap


   total n grams / length test summaries'''

def _experiment(args):
   ''' Used for chained evaluations on a single pre-trained model '''
   _ngrams()
   experiments = []
   exp1 = Experiment('Bestuursrecht', 0, 0, 'Bestuursrecht', 'data/rechtspraak/bestuursrecht_output_beam5')
   exp2 = Experiment('Civielrecht', 0, 0, 'Civiel recht', 'data/rechtspraak/civielrecht_output_beam5')
   exp3 = Experiment('Strafrecht', 0, 0, 'Strafrecht', 'data/rechtspraak/strafrecht_output_beam5')
   exp4 = Experiment('Belastingrecht', 0, 0, 'Belastingrecht', 'data/rechtspraak/belastingrecht_output_beam5')
   exp5 = Experiment('1970-2000', 1970, 2000, '-', 'data/rechtspraak/1970_2000_output_beam5')
   exp6 = Experiment('2001-2008', 2001, 2008, '-', 'data/rechtspraak/2001_2008_output_beam5')
   exp7 = Experiment('2009-2018', 2009, 2018, '-', 'data/rechtspraak/2009_2018_output_beam5')

   experiments.extend((exp1,exp2,exp3,exp4,exp5,exp6,exp7))

   # delete decode directories
   print('Deleting decode directories..')
   for exp in experiments:
      if os.path.exists(exp.decode_dir):
         shutil.rmtree(exp.decode_dir)

   # add casenumber
   print('Adding casenumbers..')
   all_cases = []
   with os.scandir(f'data/rechtspraak/finished/test_all') as data_split:
         for case in data_split:  
            with open(f'data/rechtspraak/finished/test_all/{case.name}', 'r') as f:
               data = json.loads(f.read())
               all_cases.append(data['case'])

   # add type 
   print('Adding type..')
   new_cases = {}
   for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
      for index, row in chunk.iterrows():
            if row['case'] in all_cases:
               new_cases[row['case']] = row['type']
   
   # browse through experiments
   i = 1
   with open(f'{log_dir}/experiments', 'w') as f:
      for exp in experiments:
         exp_time = time()            
         print(f'Experiment run {i} - {exp.name}...\n')
         f.write(f'Experiment run {i} - {exp.name}\n')
         f.write(f'minYear: {exp.minYear} - maxYear: {exp.maxYear} - caseType: {exp.caseType} - decode_dir: {exp.decode_dir}----\n')
         
         filtered_cases = []     
         # year filter
         if exp.minYear != 0 and exp.maxYear != 0:
            if exp.caseType != '-':
               for key, value in new_cases.items():
                  year = int(key.split(':')[3]) 
                  if year >= int(exp.minYear) and year <= int(exp.maxYear) and str(exp.caseType) in value:
                     filtered_cases.append(key)
            else:
               for key, value in new_cases.items():
                  year = int(key.split(':')[3]) 
                  if year >= int(exp.minYear) and int(year <= exp.maxYear):
                     filtered_cases.append(key)
         
         # type filter
         else:
            for key, value in new_cases.items():
               if str(exp.caseType) in str(value):
                  filtered_cases.append(key)
                  
         f.write(f'{len(filtered_cases)} cases found!\n')  
         print(f'{len(filtered_cases)} cases found!\n')      

          # delete old experiment test directory
         print('Deleting old experiment test directory..')
         if os.path.exists('data/rechtspraak/finished/test'):
            shutil.rmtree('data/rechtspraak/finished/test')
         os.makedirs('data/rechtspraak/finished/test')  

         # find files belonging to filter and copy to /test/
         print('Copying files...\n')
         j = 0
         with os.scandir(f'data/rechtspraak/finished/test_all') as data_split:
            for case in data_split:
               with open(f'data/rechtspraak/finished/test_all/{case.name}', 'r') as g:
                  data = json.loads(g.read())
                  if data['case'] in filtered_cases:
                     shutil.copy(f'data/rechtspraak/finished/test_all/{case.name}', f'data/rechtspraak/finished/test/{j}.json')
                     j += 1
         
         print("Start decoding...\n")
         beam = 5
         batch_size = 32
         decodeModel(exp.decode_dir, 'data/rechtspraak/rl_phase2/', 'test', beam, batch=batch_size)
         f.write(f'decodeModel: decode_dir {exp.decode_dir} - data_split test - beam {beam} - batch {batch_size}\n\n')

         print('Evaluation...\n')
         makeEvaluationReferences()
         rouge = evaluateFullModel(exp.decode_dir)       

         print('Exploring summaries...')
         exploreGeneratedSummaries(f, exp.decode_dir)

         f.write(f'ROUGE: \n {rouge}')
         f.write(f'Experiment {i}: {timedelta(seconds=time()-exp_time)}\n\n\n')
         print(f'Experiment {i} done!\n\n')
         i += 1

def _main(args):
   total_time = time()  
   #_preprocess(args)
   _learn(args)
   _decode(args)
   _evaluate(args)
        
   with open(f'{log_dir}/total', 'w') as f:
      f.write(f'Total time: {timedelta(seconds=time()-total_time)}')

if __name__ == '__main__':
    os.makedirs('log', exist_ok=True)
    os.makedirs(log_dir)
    
    parser = argparse.ArgumentParser()

    # Choose pipeline
    subparsers = parser.add_subparsers(help='Either perform only pre-processing (\'pre\'), deep learning (\'learn\'), decoding (\'decode\'), evaluate (\'eval\') or all (\'all\')')

    # All
    all_parser = subparsers.add_parser('all', help='Full pipeline')
    all_parser.add_argument('--output_cases', required=True, help='Output directory for verdicts and summaries')
    all_parser.add_argument('--minWords', type=int, action='store', required=True, help='Minimal number of words needed in a summary for it to be added to the training set')
    all_parser.add_argument('--maxWords', type=int, action='store',required=True, help='Maximum number of words needed in a summary for it to be added to the training set')
    all_parser.add_argument('--minSentences', type=int, action='store', required=True, help='Minimal number of sentence needed in a summary for it to be added to the training set')
    all_parser.add_argument('--maxSentences', type=int, action='store', required=True, help='Maximum number of sentence needed in a summary for it to be added to the training set')
    all_parser.add_argument('--minWordsInSentence', type=int, action='store', required=True, help='Minimal number of words needed in each summary sentence for it to be added to the training set (must be >3)')
    all_parser.add_argument('--minYear', type=int, action='store', default=0, help='Minimal year of case')
    all_parser.add_argument('--maxYear', type=int, action='store', default=0, help='Max year of case')
    all_parser.add_argument('--caseType', action='store', default='all', help='Type of case')
    all_parser.add_argument('--multiprocess', action='store_true', help='If parameter is added, multiprocessing will be used when making extraction labels. This might not work properly on low-end PCs')
    all_parser.add_argument('--dataExploration', action='store_true', help='If parameter is added, the application will print information about the selected data')    
    all_parser.add_argument('--nltk', action='store_true', help='If parameter is added, the application will use NLTK for tokenization in pre-processing') 
    all_parser.add_argument('--batch_size', type=int, action='store', default=32, help='Number of training examples utilized in one iteration for RL')
    all_parser.add_argument('--ckpt_freq', type=int, action='store', help='Number of steps needed to save a checkpoint in RL (lower=faster)')   
    all_parser.add_argument('--beam', type=int, required=True, help='Number of hypothesis for beam search (1-5)')
    all_parser.add_argument('--data_split', required=True, help='Run decoding on either test or validation set (test/val)')
    all_parser.add_argument('--decode_dir', required=True, help='Output directory for generated summaries')
    all_parser.add_argument('--decode_batch_size', type=int, required=True, help='Batch size for decoding')
    all_parser.set_defaults(func=_main)

    # Pre-processing  
    pre_parser = subparsers.add_parser('pre', help='Pre-processing')
    pre_parser.add_argument('--output_cases', required=True, help='Output directory for verdicts and summaries')
    pre_parser.add_argument('--minWords', type=int, action='store', required=True, help='Minimal number of words needed in a summary for it to be added to the training set')
    pre_parser.add_argument('--maxWords', type=int, action='store',required=True, help='Maximum number of words needed in a summary for it to be added to the training set')
    pre_parser.add_argument('--minSentences', type=int, action='store', required=True, help='Minimal number of sentence needed in a summary for it to be added to the training set')
    pre_parser.add_argument('--maxSentences', type=int, action='store', required=True, help='Maximum number of sentence needed in a summary for it to be added to the training set')
    pre_parser.add_argument('--minWordsInSentence', type=int, action='store', required=True, help='Minimal number of words needed in each summary sentence for it to be added to the training set (must be >3)')
    pre_parser.add_argument('--minYear', type=int, action='store', default=0, help='Minimal year of case')
    pre_parser.add_argument('--maxYear', type=int, action='store', default=0, help='Max year of case')
    pre_parser.add_argument('--caseType', action='store', default='all', help='Type of case')
    pre_parser.add_argument('--multiprocess', action='store_true', help='If parameter is added, multiprocessing will be used when making extraction labels. This might not work properly on low-end PCs')
    pre_parser.add_argument('--dataExploration', action='store_true', help='If parameter is added, the application will print information about the selected data') 
    pre_parser.add_argument('--nltk', action='store_true', help='If parameter is added, the application will use NLTK for tokenization in pre-processing') 
    pre_parser.set_defaults(func=_preprocess)

    # Learning
    learn_parser = subparsers.add_parser('learn', help='Learning')
    learn_parser.add_argument('--batch_size', type=int, action='store', help='Number of training examples utilized in one iteration for RL')
    learn_parser.add_argument('--ckpt_freq', type=int, action='store', help='Number of steps needed to save a checkpoint in RL(lower=faster)')
    learn_parser.set_defaults(func=_learn)

    # Decoding
    decode_parser = subparsers.add_parser('decode', help='Decoding')
    decode_parser.add_argument('--beam', type=int, required=True, help='Number of hypothesis for beam search (1-5)')
    decode_parser.add_argument('--data_split', required=True, help='Run decoding on either test or validation set (test/val)')
    decode_parser.add_argument('--decode_dir', required=True, help='Output directory for generated summaries')
    decode_parser.add_argument('--decode_batch_size', type=int, required=True, help='Batch size for decoding')
    decode_parser.set_defaults(func=_decode)

    # Experiments
    experiments_parser = subparsers.add_parser('exp', help='Experiment')
    experiments_parser.set_defaults(func=_experiment)

    # Evaluation
    eval_parser = subparsers.add_parser('eval', help='Evaluation')
    eval_parser.add_argument('--decode_dir', required=True, help='Output directory for generated summaries')
    eval_parser.set_defaults(func=_evaluate)

    args = parser.parse_args()
    args.func(args)
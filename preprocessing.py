import sys
import os
import hashlib
import subprocess
import collections

import shutil
import json
import tarfile
import io
import pickle as pkl

from os.path import join

dm_single_close_quote = '\u2019'
dm_double_close_quote = '\u201d'

# acceptable ways to end a sentence
END_TOKENS = ['.', '!', '?', '...', "'", "`", '"',
              dm_single_close_quote, dm_double_close_quote, ")", ",", ":", ";"]

train = "data/rechtspraak/train.txt"
val = "data/rechtspraak/val.txt"
test = "data/rechtspraak/test.txt"

tokenized_cases_dir = "data/rechtspraak/tokenized"
finished_cases_dir = "data/rechtspraak/finished"

def _tokenizeStories(cases_dir, tokenized_cases_dir):
    """Maps a whole directory of files to a tokenized version using
       Stanford CoreNLP Tokenizer
    """
    print("Preparing to tokenize {} to {}...".format(cases_dir,
                                                     tokenized_cases_dir))
    cases = os.listdir(cases_dir)
    # make IO list file
    print("Making list of files to tokenize...")
    with open("mapping.txt", "w") as f:
        for s in cases:
            f.write(
                "{} \t {}\n".format(
                    os.path.join(cases_dir, s),
                    os.path.join(tokenized_cases_dir, s)
                )
            )
    command = ['java', 'edu.stanford.nlp.process.PTBTokenizer',
               '-ioFileList', '-preserveLines', 'mapping.txt']
    print("Tokenizing {} files in {} and saving in {}...".format(
        len(cases), cases_dir, tokenized_cases_dir))
    subprocess.call(command)
    print("Stanford CoreNLP Tokenizer has finished.")
    os.remove("mapping.txt")

    # Check that the tokenized stories directory contains the same number of
    # files as the original directory
    num_orig = len(os.listdir(cases_dir))
    num_tokenized = len(os.listdir(tokenized_cases_dir))
    if num_orig != num_tokenized:
        raise Exception(
            "The tokenized stories directory {} contains {} files, but it "
            "should contain the same number as {} (which has {} files). Was"
            " there an error during tokenization?".format(
                tokenized_cases_dir, num_tokenized, cases_dir, num_orig)
        )
    print("Successfully finished tokenizing {} to {}.\n".format(
        cases_dir, tokenized_cases_dir))

def _readStoryFile(text_file):
    with open(text_file, "r", encoding="utf-8") as f:
        # sentences are separated by 2 newlines
        # single newlines might be image captions
        # so will be incomplete sentence
        lines = f.read().split('\n\n')
    return lines

def _fixMissingPeriod(line):
    """Adds a period to a line that is missing a period"""
    if line == "":
        return line
    if line[-1] in END_TOKENS:
        return line
    return line + " ."

def _getLines(story_file):
    """ return as list of sentences"""
    lines = _readStoryFile(story_file)

    # Lowercase, truncated trailing spaces, and normalize spaces
    lines = [' '.join(line.lower().strip().split()) for line in lines]

    # Put periods on the ends of lines that are missing them (this is a problem
    # in the dataset because many image captions don't end in periods;
    # consequently they end up in the body of the article as run-on sentences)
    lines = [_fixMissingPeriod(line) for line in lines]

    # Separate out article and abstract sentences
    file_lines = []
    for idx, line in enumerate(lines):
        if line == "":
            continue # empty line
        else:
            file_lines.append(line)

    return lines

def _writeToTar(url_file, out_file, makevocab=False):
    """Reads the tokenized .story files corresponding to the urls listed in the
       url_file and writes them to a out_file.
    """
    print("Making bin file for URLs listed in {}...".format(url_file))
    story_fnames = [line.strip() for line in open(url_file)]
    num_stories = len(story_fnames)

    if makevocab:
        vocab_counter = collections.Counter()

    with tarfile.open(out_file, 'w') as writer:
        for idx, s in enumerate(story_fnames):
            if idx % 1000 == 0:
                print("Writing story {} of {}; {:.2f} percent done".format(
                    idx, num_stories, float(idx)*100.0/float(num_stories)))

            # Look in the tokenized story dirs to find the .txt file
            # corresponding to this file
            if os.path.isfile(os.path.join(tokenized_cases_dir, f'{s}.txt')):
                case_file = os.path.join(tokenized_cases_dir, f'{s}.txt')
                summary_file = os.path.join(tokenized_cases_dir, f'{s}_SUMMARY.txt')
            else:
                print("Error: Couldn't find tokenized story file {} in"
                      " tokenized story directories {}. Was there an"
                      " error during tokenization?".format(
                          f'{s}.txt', tokenized_cases_dir))
               
            # Get the strings to write to .bin file
            verdict_sents = _getLines(case_file)
            summary_sents = _getLines(summary_file)
            
            # Write down the case in the correct format
            case = s.replace(".txt", "")
            case = case.replace("_", ":")

            # Write to JSON file
            js_example = {}
            js_example['case'] = case
            js_example['summary'] = summary_sents
            js_example['verdict'] = verdict_sents           
            js_serialized = json.dumps(js_example, indent=4).encode()
            save_file = io.BytesIO(js_serialized)
            tar_info = tarfile.TarInfo('{}/{}.json'.format(
                os.path.basename(out_file).replace('.tar', ''), idx))
            tar_info.size = len(js_serialized)
            writer.addfile(tar_info, save_file)

            # Write the vocab to file, if applicable
            if makevocab:
                art_tokens = ' '.join(verdict_sents).split()
                abs_tokens = ' '.join(summary_sents).split()
                tokens = art_tokens + abs_tokens
                tokens = [t.strip() for t in tokens] # strip
                tokens = [t for t in tokens if t != ""] # remove empty
                vocab_counter.update(tokens)

    print("Finished writing file {}\n".format(out_file))

    # write vocab to file
    if makevocab:
        print("Writing vocab file...")
        with open(os.path.join(finished_cases_dir, "vocab_cnt.pkl"),
                  'wb') as vocab_file:
            pkl.dump(vocab_counter, vocab_file)
        print("Finished writing vocab file")

def _extractTar(name):
    tar = tarfile.open(name)
    tar.extractall(finished_cases_dir)
    tar.close()

def tokenize(cases_dir):
    # Delete old directories
    if os.path.exists(tokenized_cases_dir):
        shutil.rmtree(tokenized_cases_dir)
    if os.path.exists(finished_cases_dir):
        shutil.rmtree(finished_cases_dir)

    # Create some new directories
    os.makedirs(tokenized_cases_dir)
    os.makedirs(finished_cases_dir)


    # Run stanford tokenizer on both stories dirs,
    # outputting to tokenized stories directories
    _tokenizeStories(cases_dir, tokenized_cases_dir)

    # Read the tokenized stories, do a little postprocessing
    # then write to bin files
    _writeToTar(test, os.path.join(finished_cases_dir, "test.tar"))
    _writeToTar(val, os.path.join(finished_cases_dir, "val.tar"))
    _writeToTar(train, os.path.join(finished_cases_dir, "train.tar"), makevocab=True)

    # Extract tar files in current directory
    _extractTar(os.path.join(finished_cases_dir, "test.tar"))
    _extractTar(os.path.join(finished_cases_dir, "val.tar"))
    _extractTar(os.path.join(finished_cases_dir, "train.tar"))
import os
import argparse
import errno
import csv
import math
import json

import nltk
import nltk.data
import ucto

import pandas as pd

from os.path import join
from time import time
from datetime import timedelta

sentence_detector = nltk.data.load('tokenizers/punkt/dutch.pickle')
csv_file = 'data/rechtspraak/final.csv'
tokenizer = ucto.Tokenizer('tokconfig-nld-historical', sentenceperlineoutput=True, sentencedetection=True, paragraphdetection=False)

def _splitSentenceNltk(text):
    text = text.strip()
    return sentence_detector.tokenize(text)

def _countVerdictWordsAndSentences(verdict):
    ''' Only counts REAL words, no punctation/symbols '''
    verdict_paragraphs = verdict.split('\\n')

    word_count = 0
    sentence_count = 0
    for paragraph in verdict_paragraphs:
        tokenizer.process(paragraph)
        
        for token in tokenizer:
            if token.tokentype != 'PUNCTUATION' and token.tokentype != 'SYMBOL':
                word_count += 1
            if token.isendofsentence():
                sentence_count += 1

    return word_count, sentence_count

def _countWordsAndSentences(text):
    ''' Only counts REAL words, no punctation/symbols '''
    tokenizer.process(text)

    word_count = 0
    sentence_count = 0
    for token in tokenizer:
        if token.tokentype != 'PUNCTUATION' and token.tokentype != 'SYMBOL':
            word_count += 1
        if token.isendofsentence():
            sentence_count += 1
    
    return word_count, sentence_count

def _determineIndividualTypeDistribution(types):
    strafrecht, civielrecht, bestuursrecht, belastingrecht = 0,0,0,0 # biggest categories
    for key, value in types.items(): 
        if 'Bestuursrecht' in key:
            bestuursrecht += value
        if 'Civiel recht' in key:
            civielrecht += value  
        if 'Strafrecht' in key:
            strafrecht += value
        if 'Belastingrecht' in key:
            belastingrecht += value
    
    return strafrecht, civielrecht, bestuursrecht, belastingrecht

def _addToYearDistribution(years, caseNumber):
    year = caseNumber.split(':')[3]  # ECLI:NL:CRVB:1976:AM4241
    if (year in years):
        years[year] += 1
    else:
        years[year] = 1

def _addToTypeDistribution(types, caseType):
    if (caseType in types):
        types[caseType] += 1
    else:
        types[caseType] = 1

def _addToWordDistribution(distribution, words):
    if words < 10:
        distribution[0] += 1
    elif words >= 10 and words < 25:
        distribution[1] += 1
    elif words >= 25 and words < 50:
        distribution[2] += 1
    elif words >= 50 and words < 75:
        distribution[3] += 1
    elif words >= 75 and words < 100:
        distribution[4] += 1
    elif words >= 100 and words < 150:
        distribution[5] += 1
    elif words >= 150 and words < 250:
        distribution[6] += 1
    elif words >= 250 and words < 400:
        distribution[7] += 1
    elif words > 400:
        distribution[8] += 1

def _writeDistribution(f, distribution):
    f.write(f'< 10: {distribution[0]} cases\n')      
    f.write(f'>= 10 and < 25: {distribution[1]} cases\n')
    f.write(f'>= 25 and < 50: {distribution[2]} cases\n')
    f.write(f'>= 50 and < 75: {distribution[3]} cases\n')
    f.write(f'>= 75 and < 100: {distribution[4]} cases\n')
    f.write(f'>= 100 and < 150: {distribution[5]} cases\n')
    f.write(f'>= 150 and <250: {distribution[6]} cases\n')
    f.write(f'>= 250 and < 400: {distribution[7]} cases\n')
    f.write(f'>= 400: {distribution[8]} cases\n')

def _writeSentenceDistribution(f, distribution):
    f.write(f'1 sentence: {distribution[0]} cases \n')
    f.write(f'2 sentence: {distribution[1]} cases \n')
    f.write(f'3 sentence: {distribution[2]} cases \n')
    f.write(f'4 sentence: {distribution[3]} cases \n')
    f.write(f'5 sentence: {distribution[4]} cases \n')
    f.write(f'6 sentence: {distribution[5]} cases \n')
    f.write(f'7 sentence: {distribution[6]} cases \n')
    f.write(f'8 sentence: {distribution[7]} cases \n')
    f.write(f'9-15 sentence: {distribution[8]} cases \n')
    f.write(f'15+ sentence: {distribution[9]} cases \n')

def _addToSentenceDistribution(distribution, sentences):
    if sentences == 1:
        distribution[0] += 1
    elif sentences == 2:
        distribution[1] += 1
    elif sentences == 3:
        distribution[2] += 1
    elif sentences == 4:
        distribution[3] += 1
    elif sentences == 5:
        distribution[4] += 1
    elif sentences == 6:
        distribution[5] += 1
    elif sentences == 7:
        distribution[6] += 1
    elif sentences == 8:
        distribution[7] += 1
    elif sentences >= 9 and sentences <= 15:
        distribution[8] += 1
    elif sentences > 15:
        distribution[9] += 1    

def exploreCsv(f):
    cases, cases_words, cases_sentences, summaries, summaries_case_words,  summaries_case_sentences, summaries_words, summaries_sentences, = 0,0,0,0,0,0,0,0
    types, years = {},{}
    general_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    bestuursrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    civielrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    strafrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    belastingrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0] 
    
    for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
        cases += len(chunk)
        for index, row in chunk.iterrows():  
            if index % 1000 == 0:
                print(f'Explore before pre-processing: {index} cases finished...', end='\r')
            
            verdict = row['verdict'].strip()
            case_words, case_sentences = _countVerdictWordsAndSentences(verdict)
            cases_words += case_words
            cases_sentences += case_sentences
    
            # found a NaN summary
            if (type(row['summary']) is float and math.isnan(row['summary'])):           
                continue            
                
            summary = row['summary'].strip()
                    
            # found an empty summary
            if summary == "-" or summary == "":
                continue
            
            # found a real summary
            else:
                summaries += 1              
                summaries_case_words += case_words
                summaries_case_sentences += case_sentences    
                summary_words, summary_sentences = _countWordsAndSentences(summary)
                summaries_words += summary_words
                summaries_sentences += summary_sentences

                _addToWordDistribution(general_summary_word_distribution, summary_words)
                _addToTypeDistribution(types, row['type'])
                _addToYearDistribution(years, row['case'])

                if 'Bestuursrecht' in row['type']:
                    _addToWordDistribution(bestuursrecht_summary_word_distribution, summary_words)
                if 'Civiel recht' in row['type']:
                    _addToWordDistribution(civielrecht_summary_word_distribution, summary_words)  
                if 'Strafrecht' in row['type']:
                    _addToWordDistribution(strafrecht_summary_word_distribution, summary_words)
                if 'Belastingrecht' in row['type']:
                    _addToWordDistribution(belastingrecht_summary_word_distribution, summary_words)
    
    strafrecht, civielrecht, bestuursrecht, belastingrecht = _determineIndividualTypeDistribution(types)

    if cases == 0:
        f.write('No cases found.\n')
        return

    f.write('BEFORE PRE-PROCESSING EXPLORATION\n')
    f.write('----------------------------------------------------\n')
    f.write('General info:\n\n')
    f.write(f'Number of cases: {cases}\n')
    f.write(f'Number of words in all cases: {cases_words}\n')
    f.write(f'Number of sentences in all cases: {cases_sentences}\n')
    f.write(f'Average number of words in a case: {round(cases_words/cases, 2)}\n')
    f.write(f'Average number of sentences in a case: {round(cases_sentences/cases, 2)}\n')
    f.write(f'Average number of words in a sentence in a case: {round(cases_words/cases_sentences, 2)}\n')
    f.write(f'Number of cases with a summary: {summaries}\n')
    f.write(f'Number of words in all cases with a summary: {summaries_case_words}\n')
    f.write(f'Number of sentences in all cases with a summary: {summaries_case_sentences}\n')
    f.write(f'Average number of words in a case with a summary: {round(summaries_case_words/summaries, 2)}\n')
    f.write(f'Average number of sentences in a case with a summary: {round(summaries_case_sentences/summaries, 2)}\n')
    f.write(f'Average number of words in a sentence in a case with a summary: {round(summaries_case_words/summaries_case_sentences, 2)}\n')
    f.write(f'Number of words in all summaries: {summaries_words}\n')
    f.write(f'Number of sentences in all summaries: {summaries_sentences}\n')
    f.write(f'Average number of words in a summary: {round(summaries_words/summaries, 2)}\n')
    f.write(f'Average number of sentences in a summary: {round(summaries_sentences/summaries, 2)}\n')
    f.write(f'Average number of words in a sentence in a summary: {round(summaries_words/summaries_sentences, 2)}\n')  
    f.write('\n\n')

    f.write('Summary word distribution\n\n')
    _writeDistribution(f, general_summary_word_distribution)
    f.write('\n\n')

    f.write('Type distribution\n\n')
    for key, value in [(k, types[k]) for k in sorted(types, key=types.get, reverse=True)]:
        f.write(f'{key}: {round(value / cases * 100, 2)}%\n')
    f.write('\n\n')

    f.write('Individual type distribution\n\n')
    f.write(f'Strafrecht: {round(strafrecht / cases * 100, 2)}%\n')
    f.write(f'Civielrecht: {round(civielrecht / cases * 100, 2)}%\n')
    f.write(f'Bestuursrecht: {round(bestuursrecht / cases * 100, 2)}%\n')
    f.write(f'Belastingrecht: {round(belastingrecht / cases * 100, 2)}%\n')
    f.write('\n\n')

    f.write('Strafrecht summary word distribution\n\n')
    _writeDistribution(f, strafrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Civielrecht summary word distribution\n\n')
    _writeDistribution(f, civielrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Bestuursrecht word distribution\n\n')
    _writeDistribution(f, bestuursrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Belastingrecht word distribution\n\n')
    _writeDistribution(f, belastingrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Year distribution\n\n')
    for key in sorted(years): 
        f.write(f'{key} - {round(years[key] / cases * 100, 2)}%\n')
        
    f.write('----------------------------------------------------\n\n')     

def exploreCases(f, cases_dir):
    general_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    cases, cases_words, cases_sentences, summaries,  summaries_words, summaries_sentences, = 0,0,0,0,0,0
    casenumbers = {}
    types, years = {},{}
    bestuursrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    civielrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    strafrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0]
    belastingrecht_summary_word_distribution = [0,0,0,0,0,0,0,0,0] 

    index = 0
    with os.scandir(f'{cases_dir}') as entries:
        for entry in entries:       
            index += 1
            if index % 10 == 0:
                print(f'Explore after pre-processing: {index} cases finished...', end='\r')

            with open(f'{cases_dir}/{entry.name}') as fp:   
                if entry.name.endswith('_SUMMARY.txt'):
                    summaries += 1
                    summary_words = 0

                    for index, line in enumerate(fp): 
                        if (line != '\n'):
                            summaries_sentences += 1                                    
                            words, _ = _countWordsAndSentences(line)       
                            summaries_words += words
                            summary_words += words

                    case_number = entry.name[:-12].replace('_', ':')
                    casenumbers[case_number] = summary_words

                    _addToYearDistribution(years, case_number)
                    _addToWordDistribution(general_summary_word_distribution, summary_words)

                else:
                    cases += 1            
                    for index, line in enumerate(fp):   
                        if (line != '\n'):
                            cases_sentences += 1
                            words, _ = _countWordsAndSentences(line)
                            cases_words += words

    for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
        for index, row in chunk.iterrows():
            if index % 500 == 0:
                    print(f'Explore case types after pre-processing: {index} cases finished...', end='\r')

            if row['case'] in casenumbers:
                summary_words = casenumbers[row['case']]
                _addToTypeDistribution(types, row['type'])

                if 'Bestuursrecht' in row['type']:
                    _addToWordDistribution(bestuursrecht_summary_word_distribution, summary_words)
                if 'Civiel recht' in row['type']:
                    _addToWordDistribution(civielrecht_summary_word_distribution, summary_words)  
                if 'Strafrecht' in row['type']:
                    _addToWordDistribution(strafrecht_summary_word_distribution, summary_words)
                if 'Belastingrecht' in row['type']:
                    _addToWordDistribution(belastingrecht_summary_word_distribution, summary_words)
       
    strafrecht, civielrecht, bestuursrecht, belastingrecht = _determineIndividualTypeDistribution(types)

    assert summaries == cases

    if cases == 0:
        f.write('No cases found.\n')
        return

    f.write('AFTER PRE-PROCESSING EXPLORATION\n')
    f.write('----------------------------------------------------\n')
    f.write('General info:\n\n')
    f.write(f'Number of cases: {cases}\n')
    f.write(f'Number of words in all cases: {cases_words}\n')
    f.write(f'Number of sentences in all cases: {cases_sentences}\n')
    f.write(f'Average number of words in a case: {round(cases_words/cases, 2)}\n')
    f.write(f'Average number of sentences in a case: {round(cases_sentences/cases, 2)}\n')
    f.write(f'Average number of words in a sentence in a case: {round(cases_words/cases_sentences, 2)}\n')
   
    f.write(f'Number of words in all summaries: {summaries_words}\n')
    f.write(f'Number of sentences in all summaries: {summaries_sentences}\n')
    f.write(f'Average number of words in a summary: {round(summaries_words/summaries, 2)}\n')
    f.write(f'Average number of sentences in a summary: {round(summaries_sentences/summaries, 2)}\n')
    f.write(f'Average number of words in a sentence in a summary: {round(summaries_words/summaries_sentences, 2)}\n')  
    f.write('\n\n')

    f.write('Summary word distribution\n\n')
    _writeDistribution(f, general_summary_word_distribution)
    f.write('\n\n')

    f.write('Type distribution\n\n')
    for key, value in [(k, types[k]) for k in sorted(types, key=types.get, reverse=True)]:
        f.write(f'{key}: {round(value / cases * 100, 2)}%\n')
    f.write('\n\n')

    f.write('Individual type distribution\n\n')
    f.write(f'Strafrecht: {round(strafrecht / cases * 100, 2)}%\n')
    f.write(f'Civielrecht: {round(civielrecht / cases * 100, 2)}%\n')
    f.write(f'Bestuursrecht: {round(bestuursrecht / cases * 100, 2)}%\n')
    f.write(f'Belastingrecht: {round(belastingrecht / cases * 100, 2)}%\n')
    f.write('\n\n')

    f.write('Strafrecht summary word distribution\n\n')
    _writeDistribution(f, strafrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Civielrecht summary word distribution\n\n')
    _writeDistribution(f, civielrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Bestuursrecht word distribution\n\n')
    _writeDistribution(f, bestuursrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Belastingrecht word distribution\n\n')
    _writeDistribution(f, belastingrecht_summary_word_distribution)
    f.write('\n\n')

    f.write('Year distribution\n\n')
    for key in sorted(years): 
        f.write(f'{key} - {round(years[key] / cases * 100, 2)}%\n')

    f.write('----------------------------------------------------\n\n') 

def exploreGeneratedSummaries(f, decode_dir):
    sentence_distribution = [0,0,0,0,0,0,0,0,0,0]
    word_distribution = [0,0,0,0,0,0,0,0,0]
    summaries, summaries_words, summaries_sentences = 0,0,0

    with os.scandir(f'{decode_dir}/output_with_casenumber/') as entries:
        for entry in entries:       
            with open(f'{decode_dir}/output_with_casenumber/{entry.name}') as fp:
                summaries += 1
                sentences = 0
                words = 0
                for index, line in enumerate(fp):                    
                    if (line != '\n'):
                        sentences += 1
                        line_words, _ = _countWordsAndSentences(line)
                        words += line_words
                    
                _addToWordDistribution(word_distribution, words)
                _addToSentenceDistribution(sentence_distribution, sentences)
                summaries_words += words
                summaries_sentences += sentences

    f.write('GENERATED SUMMARIES EXPLORATION\n')
    f.write('----------------------------------------------------\n')

    f.write(f'Number of words in all summaries: {summaries_words}\n')
    f.write(f'Number of sentences in all summaries: {summaries_sentences}\n')
    f.write(f'Average number of words in a summary: {round(summaries_words/summaries, 2)}\n')
    f.write(f'Average number of sentences in a summary: {round(summaries_sentences/summaries, 2)}\n')
    f.write(f'Average number of words in a sentence in a summary: {round(summaries_words/summaries_sentences, 2)}\n')  
    
    f.write('Summary word distribution\n\n')
    _writeDistribution(f, word_distribution)
    f.write('\n\n')

    f.write('Summary sentence distribution\n\n')
    _writeSentenceDistribution(f, sentence_distribution)
    f.write('\n\n')    

    f.write('----------------------------------------------------\n\n') 
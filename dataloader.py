import os
import csv
import math
import json
import shutil
import nltk
import nltk.data
import ucto

import pandas as pd

from random import shuffle

sentence_detector = nltk.data.load('tokenizers/punkt/dutch.pickle')
csv_file = 'data/rechtspraak/final.csv'
tokenizer = ucto.Tokenizer('tokconfig-nld-historical', sentenceperlineoutput=True, sentencedetection=True, paragraphdetection=False)

def _splitSentenceNltk(text):
    text = text.strip()
    return sentence_detector.tokenize(text)

def _splitSentencesUcto(text):
    ''' Tokenizes as well 
        doesnt count punctation/symbols
    '''
    tokenizer.process(text)

    sentences_length = []
    sentences = []
    sentence = ''
    count = 0
    for token in tokenizer:
        if token.tokentype != 'PUNCTUATION' and token.tokentype != 'SYMBOL':
                count += 1

        sentence += str(token) + ' '
       
        if token.isendofsentence():
            sentences.append(sentence.strip())
            sentences_length.append(count)
            sentence = ''
            count = 0
    
    return sentences, sentences_length

def _splitVerdictSentencesUcto(verdict):
    verdict_paragraphs = verdict.split('\\n')
    verdict = []

    for paragraph in verdict_paragraphs:
        sentences, _ = _splitSentencesUcto(paragraph)
        verdict.extend(sentences)

    return verdict

def _retrieveCasesNLtkLegacy(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType):
    cases = []
    
    for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
        for index, row in chunk.iterrows():        
            stop = False
           
            if index % 1000 == 0:
                print(f'Dataloader: {index} cases finished...', end='\r')

            # found a NaN summary
            if (type(row['summary']) is float and math.isnan(row['summary'])):           
                continue            
                
            summary = row['summary'].strip()
                    
            # found an empty summary
            if summary == "-" or summary == "":
                continue
            
            # found a real summary
            else:
                summaries_words = summary.count(' ') + 1
                summaries_sentences = _splitSentenceNltk(summary)
                verdict = row['verdict'].split('\\n')
                
                if (summaries_words >= minWords and summaries_words < maxWords and 
                    len(summaries_sentences) >= minSentences and len(summaries_sentences) <= maxSentences):
                    
                    for sentence in summaries_sentences:
                        if stop:
                            break
                        if ((sentence.count(' ') + 1) < minWordsInSentence):
                            stop = True
                        
                    if stop:
                        continue
                            
                            
                    
                    # ':' is not allowed in a filename
                    case = row['case'].replace(':', '_')
                    
                    # Write verdict
                    with open(f'{path}/{case}.txt', 'w', encoding='utf-8') as file:
                        verdict = row['verdict'].split('\\n')
                        
                        for sentence in verdict:
                            file.write(sentence)        
                            if sentence != verdict[len(verdict) -1]:
                                file.write('\n\n')

                    # Write summary
                    with open(f'{path}/{case}_SUMMARY.txt', 'w', encoding='utf-8') as file:
                        for sentence in summaries_sentences:
                            file.write(sentence)                     
                            if sentence != summaries_sentences[len(summaries_sentences) -1]:
                                file.write('\n\n')
                            
                    # Write casenumbers          
                    cases.append(case) 

    return cases      

def _retrieveCasesNltk(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType):
    cases = []
    
    for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
        for index, row in chunk.iterrows():   
            stop = False    

            if index % 1000 == 0:
                print(f'Dataloader: {index} cases finished...', end='\r')

            # found a NaN summary
            if (type(row['summary']) is float and math.isnan(row['summary'])):           
                continue            
                
            summary = row['summary'].strip()
                    
            # found an empty summary
            if summary == "-" or summary == "":
                continue
            
            # found a real summary
            else:                     
                summary_sentences = _splitSentenceNltk(summary)
                summaries_words = summary.count(' ') + 1

                year = int(row['case'].split(':')[3])
                
                 # Check filters
                if (summaries_words >= minWords and summaries_words <= maxWords and            
                   len(summary_sentences) >= minSentences and len(summary_sentences) <= maxSentences):

                    for sentence in summary_sentences:
                        if stop:
                            break
                        if ((sentence.count(' ') + 1) < minWordsInSentence):
                            stop = True
                        
                    if stop:
                        continue
         
                    if (minYear != 0 or maxYear != 0) and (year < minYear or year > maxYear):
                            continue

                    if (caseType != 'all') and (caseType not in row['type']):
                        continue
                    
                    verdict_sentences = row['verdict'].split('\\n')
                    
                    # ':' is not allowed in a filename
                    case = row['case'].replace(':', '_')
                    
                    # Write verdict
                    with open(f'{path}/{case}.txt', 'w', encoding='utf-8') as file:                 
                        for sentence in verdict_sentences:
                            file.write(sentence)        
                            if sentence != verdict_sentences[len(verdict_sentences) -1]:
                                file.write('\n\n')

                    # Write summary
                    with open(f'{path}/{case}_SUMMARY.txt', 'w', encoding='utf-8') as file:
                        for sentence in summary_sentences:
                            file.write(sentence)                     
                            if sentence != summary_sentences[len(summary_sentences) -1]:
                                file.write('\n\n')

                    # Write casenumbers          
                    cases.append(case) 

    return cases      

def _retrieveCasesUcto(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType):
    cases = []
    
    for chunk in pd.read_csv(csv_file, sep=',', chunksize=10000):
        for index, row in chunk.iterrows():        
            if index % 1000 == 0:
                print(f'Dataloader: {index} cases finished...', end='\r')

            # found a NaN summary
            if (type(row['summary']) is float and math.isnan(row['summary'])):           
                continue            
                
            summary = row['summary'].strip()
                    
            # found an empty summary
            if summary == "-" or summary == "":
                continue
            
            # found a real summary
            else:                     
                summary_sentences, summary_sentences_length = _splitSentencesUcto(summary)
                summaries_words = sum(summary_sentences_length)     

                year = int(row['case'].split(':')[3])
                
                 # Check filters
                if (summaries_words >= minWords and summaries_words <= maxWords and            
                   len(summary_sentences_length) >= minSentences and len(summary_sentences_length) <= maxSentences and
                   all(x >= minWordsInSentence for x in summary_sentences_length)):
                            
                    if (minYear != 0 or maxYear != 0) and (year < minYear or year > maxYear):
                            continue

                    if (caseType != 'all') and (caseType not in row['type']):
                        continue

                    verdict_sentences = _splitVerdictSentencesUcto(row['verdict'])
                    
                    # ':' is not allowed in a filename
                    case = row['case'].replace(':', '_')
                    
                    # Write verdict
                    with open(f'{path}/{case}.txt', 'w', encoding='utf-8') as file:                 
                        for sentence in verdict_sentences:
                            file.write(sentence)        
                            if sentence != verdict_sentences[len(verdict_sentences) -1]:
                                file.write('\n\n')

                    # Write summary
                    with open(f'{path}/{case}_SUMMARY.txt', 'w', encoding='utf-8') as file:
                        for sentence in summary_sentences:
                            file.write(sentence)                     
                            if sentence != summary_sentences[len(summary_sentences) -1]:
                                file.write('\n\n')

                    # Write casenumbers          
                    cases.append(case) 

    return cases      

def _retrieveCases(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType, nltk):
    if nltk:
        return _retrieveCasesNltk(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType)
    else:
        return _retrieveCasesUcto(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType)

def _determineDataSplit(train_percentage, val_percentage, test_percentage, cases):
    train_split, val_split, test_split = [],[],[]
    
    for index, value in enumerate(cases):
            if len(train_split) < math.ceil(train_percentage*len(cases)):
                train_split.append(value)                            
            elif len(val_split) < math.ceil(val_percentage*len(cases)): 
                val_split.append(value)
            elif len(test_split) < math.ceil(test_percentage*len(cases)):
                test_split.append(value)
    
    return train_split, val_split, test_split

def _writeCaseNumbers(path, cases):
    with open(path, 'w', encoding='utf-8') as file:
        for casenumber in cases:
            file.write(casenumber)                     
            if casenumber != cases[len(cases) -1]:
                file.write('\n')

def loadAndFilterData(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType, nltk):
    """ Reads data from final.csv found in data/rechtspraak/ """
  
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)    

    cases = _retrieveCases(path, minWords, maxWords, minSentences, maxSentences, minWordsInSentence, minYear, maxYear, caseType, nltk)   
    
    shuffle(cases)              
    train_split, val_split, test_split = _determineDataSplit(0.7, 0.15, 0.15, cases)
    
    _writeCaseNumbers('data/rechtspraak/train.txt', train_split)
    _writeCaseNumbers('data/rechtspraak/val.txt', val_split)
    _writeCaseNumbers('data/rechtspraak/test.txt', test_split)